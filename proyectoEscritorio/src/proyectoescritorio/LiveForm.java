
package proyectoescritorio;


import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;


public class LiveForm extends javax.swing.JFrame {
    
    private ArrayList<Panel_mesa> paneles;  
   
    //creo enlace a la actividad anterior así poder volver y usar la misma versión de la misma 
    private Home enlaceHome;
    
    //variables necesarias
    private int salida = 0;    
    public int posicion = 1;
    
    //panel personalziado para así añadir foto de fondo
    public Panel_mesa panel;
    
    //constructor    
    public LiveForm(Home home) {
        
        initComponents();
        
        //setLogo esquina izquierda
        setIconImage(new ImageIcon(getClass().getResource("/images/logo2.png")).getImage());
         
        //posicionamiento centrado
        this.setLocationRelativeTo(null);
        enlaceHome = home;
        
        paneles = new ArrayList<>();        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        button_back = new javax.swing.JButton();
        button_cerrar_dia = new javax.swing.JButton();
        button_start_hilos = new javax.swing.JButton();
        button_stop_hilos = new javax.swing.JButton();
        panel_encima_scrolll = new javax.swing.JScrollPane();
        panel_encima_scroll = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setExtendedState(6);

        button_back.setText("Back");
        button_back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                button_backActionPerformed(evt);
            }
        });

        button_cerrar_dia.setText("Cerrar Día");
        button_cerrar_dia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                button_cerrar_diaActionPerformed(evt);
            }
        });

        button_start_hilos.setText("Start");
        button_start_hilos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                button_start_hilosActionPerformed(evt);
            }
        });

        button_stop_hilos.setText("Stop");
        button_stop_hilos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                button_stop_hilosActionPerformed(evt);
            }
        });

        panel_encima_scrolll.setPreferredSize(new java.awt.Dimension(10, 10));

        panel_encima_scroll.setLayout(new java.awt.GridLayout(0, 4, 1, 2));
        panel_encima_scrolll.setViewportView(panel_encima_scroll);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(button_back)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 409, Short.MAX_VALUE)
                        .addComponent(button_start_hilos)
                        .addGap(33, 33, 33)
                        .addComponent(button_stop_hilos)
                        .addGap(29, 29, 29)
                        .addComponent(button_cerrar_dia))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(panel_encima_scrolll, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel_encima_scrolll, javax.swing.GroupLayout.DEFAULT_SIZE, 719, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(button_back)
                    .addComponent(button_cerrar_dia)
                    .addComponent(button_start_hilos)
                    .addComponent(button_stop_hilos))
                .addGap(18, 18, 18))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    //recibe el ArrayList con las mesas ordenadas 
    //dibuja los paneles_mesa y guarda la información de cada pedido
    public void ejecutar(ArrayList<Productos> ordenes){            
        
        if(ordenes.size() > 0){        
                        
            panel = new Panel_mesa();                                                             
            
            //elimino platos pero sumo en +1 el plato repetido 
            eliminar_duplicados(ordenes);                                   
            
            //captura la informacion sobre los platos ordenados 
            panel.capturar_info(ordenes);                                                                                                                                                                 
            
            //determina la posicion del siguiente panel
            //hubiera sido ideal el funcionamiento de FlowLayout y ScrollPanel,no hubo manera             
            //de ahí la división entre 2 paneles para poder ocupar 2 filas de mesas 
           
            panel_encima_scroll.add(panel);                                                                               
                                  
            this.posicion++;                                                 
            
            //guardo los paneles en un ArrayList, guardo los platos y refresco el panel 
            paneles.add(panel);                         
            panel.addPlatos();  
            panel_encima_scroll.updateUI();
                        
            ordenes.clear();
        }                      
    }     
    
    //elimino platos pero sumo en +1 el plato repetido 
    public void eliminar_duplicados(ArrayList<Productos>ordenes){
   
        ArrayList<Integer>borrables = new ArrayList<>();

        //recorro ordenes
        for (int i = 0; i < ordenes.size(); i++){
            for (int j = i+1; j < ordenes.size(); j++){    
                
                //hago un equals de Objetos y sumo platos repetidos en caso de equals
                if(ordenes.get(i).equals(ordenes.get(j))){                    
                    ordenes.get(i).setNumero_platos_repetidos(1);                    
                    //si el indice j no existe en la lista de borrables que lo añada
                    if(borrables.indexOf(j) == -1){
                        borrables.add(j);
                    }                   
                }
            }
        } 
        //ordeno el ArrayList así al borrar no altero el orden y no borro el que no toca 
        Collections.sort(borrables);
         
        //recorriendo al revés el ArrayList no se mueven de sitio los indices buscados 
        //Ej: indices buscados 2, 7, 9 -si borrara el 2 primero el 7 pasaría a ser 6 , y cuando llegue el turno de 9 sería 7
        for (int f = borrables.size()-1; f >= 0; f--){
            ordenes.remove((int)borrables.get(f));
        }               
    }
    
    //dejo atrás el Frame actual y vuelvo al anterior 
    private void button_backActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_button_backActionPerformed
        enlaceHome.setVisible(true);
        dispose();
    }//GEN-LAST:event_button_backActionPerformed

    //hago un set a 0 del AUTO_INCREMENTAL de la base de datos al final del día para evitar que suba hasta el infinito 
    private void button_cerrar_diaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_button_cerrar_diaActionPerformed
        Conector_bbdd conexion = new Conector_bbdd();
        
        button_cerrar_dia.setText("Cerrado");
        
        conexion.resetAuto();
    }//GEN-LAST:event_button_cerrar_diaActionPerformed
    
    //este metodo es el que hace la conexion entre la comprobación de la bbdd y la manipulación de los datos 
    public void comprobar_update(){        
       
        ArrayList<Productos> ordenes = new ArrayList<>();      
        
        Conector_bbdd conexion = new Conector_bbdd();               
                
        //el nuevo ArrayList creado capta lso datos devueltos por la bbdd
        ordenes = conexion.obtener_ordenes();        
         
        //compruebo que haya valores en el ArrayList 
        if(ordenes.size() > 0){
            for(int i = 0; i < ordenes.size(); i++){
               
                System.out.println(ordenes.get(i).getNombre());
                             
                //como ha encontrado algo en la bbdd 
                //ya está guardado en el ArrayList de ordenes y procedo a su borrado
                conexion.borrar_reserva(ordenes.get(0).getNumero()); 
            }
        }       
        //cierro conexión
        conexion.close();    
        
        //mando el ArrayList al metodo ejecutar 
        ejecutar(ordenes);        
    }
    
    //este boton es el que empieza el funcionamiento de los hilos 
    //realiza una comprobación por segundo a la base de datos 
    //también usa el .sleep para actualizar los tiempos de mesa
    private void button_start_hilosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_button_start_hilosActionPerformed
        
        salida = 0; 
        
        //mando información a los botones para hacer visual la puesta en marcha de los hilos y su parada
        button_start_hilos.setText("Started");
        button_stop_hilos.setText("Stop");
        
        Thread t1 = new Thread(new Runnable(){
           
            public void run(){
                do{ // crea bucle infinito
                    
                    try{
                        
                        System.out.println("Comprobando..."); 
                        Thread.sleep(1000); // Espera 1 segundo
                        
                        //llama al metodo 
                        comprobar_update();
                        
                        //si encuentra algun panel hace update
                        if(paneles.size() != 0){
                            for(int i = 0; i < paneles.size(); i++){
                                paneles.get(i).updateTimeOpenFor();
                                if(paneles.get(i).isCompletado()){
                                    panel_encima_scroll.remove(paneles.get(i));
                                    panel_encima_scroll.updateUI();
                                }
                            }
                        }                       
                    }
                    catch (InterruptedException ex) {
                        Logger.getLogger(LiveForm.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                }while(salida != 1);    //mientras no se pulse el boton salir seguirá trabajando
            } 
        });
        t1.start(); // Inicializa el hilo
    }//GEN-LAST:event_button_start_hilosActionPerformed
    
    //detiene los hilos y toda la actualización del programa 
    //supuestamente es para el final del día 
    private void button_stop_hilosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_button_stop_hilosActionPerformed
        salida = 1;
        System.out.println("Actualización detenida");
        button_start_hilos.setText("Start");
        button_stop_hilos.setText("Stopped");
        
    }//GEN-LAST:event_button_stop_hilosActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton button_back;
    private javax.swing.JButton button_cerrar_dia;
    private javax.swing.JButton button_start_hilos;
    private javax.swing.JButton button_stop_hilos;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel panel_encima_scroll;
    private javax.swing.JScrollPane panel_encima_scrolll;
    // End of variables declaration//GEN-END:variables
}
