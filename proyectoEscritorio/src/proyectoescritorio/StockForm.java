
package proyectoescritorio;

import java.util.ArrayList;
import javax.swing.ImageIcon;

public class StockForm extends javax.swing.JFrame {

    //creo enlace al Jframe anterios y las variables necesarias 
    private Home enlaceHome;
    String[][] info;
    String[][] info2;
    
    //constructor
    public StockForm(Home home) {
        initComponents();
        
        //Logo Jframe, locacion centrada 
        setIconImage(new ImageIcon(getClass().getResource("/images/logo2.png")).getImage());      
        this.setLocationRelativeTo(null);
        enlaceHome = home;
                
        obtenerStock();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        button_back = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        button_validar_pedido = new javax.swing.JButton();
        button_anyadir_pedido = new javax.swing.JButton();
        label_stock_actual_actualizado = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabla_ingredientes = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(237, 212, 172));

        button_back.setText("Back");
        button_back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                button_backActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Segoe Script", 1, 36)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Stock");

        button_validar_pedido.setText("Validar");
        button_validar_pedido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                button_validar_pedidoActionPerformed(evt);
            }
        });

        button_anyadir_pedido.setText("Add Pedido");
        button_anyadir_pedido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                button_anyadir_pedidoActionPerformed(evt);
            }
        });

        label_stock_actual_actualizado.setFont(new java.awt.Font("Segoe Script", 1, 18)); // NOI18N
        label_stock_actual_actualizado.setForeground(new java.awt.Color(0, 0, 0));
        label_stock_actual_actualizado.setText("STOCK ACTUAL");

        tabla_ingredientes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Nombre", "Cantidad", "Fecha Caducidad"
            }
        ));
        tabla_ingredientes.setRowHeight(30);
        jScrollPane1.setViewportView(tabla_ingredientes);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(129, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(button_back)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(button_anyadir_pedido)
                                .addGap(101, 101, 101)
                                .addComponent(button_validar_pedido, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(125, 125, 125))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(label_stock_actual_actualizado, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(219, 219, 219))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(252, 252, 252))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(73, 73, 73)
                .addComponent(label_stock_actual_actualizado, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(button_validar_pedido)
                    .addComponent(button_back)
                    .addComponent(button_anyadir_pedido))
                .addContainerGap(50, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    //vuelve al JFrame anterior 
    private void button_backActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_button_backActionPerformed
        enlaceHome.setVisible(true);
        dispose();
    }//GEN-LAST:event_button_backActionPerformed

    //captura la informacion introducida en la tabla y si es correcta ejecuta una consulta y lo graba en bbdd
    private void button_validar_pedidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_button_validar_pedidoActionPerformed
        
        Conector_bbdd conexion = new Conector_bbdd();
               
        ArrayList<productoStock> productosActualizados = new ArrayList<>();
       
        String [][] info3= new String[tabla_ingredientes.getRowCount()][tabla_ingredientes.getColumnCount()];                
        
        //captura los datos de la tabla 
        for (int i = 0; i < (tabla_ingredientes.getRowCount()); i++) {
           
            info3[i][0] = (String)( tabla_ingredientes.getValueAt(i, 0)); 
            info3[i][1] = (String)( tabla_ingredientes.getValueAt(i, 1)); 
            info3[i][2] = (String)( tabla_ingredientes.getValueAt(i, 2)); 
                        
        }    
                
        //recorro el vector y compruebo que no haya campos sin completar o solo alguna columna 
        for (String[] info31 : info3){
            if (info31[1] != null && info31[2] != null) {
               
                //creo un temporal de tipo productoStock
                productoStock temporal = new productoStock();
                
                temporal.setNombre(info31[0]);
                temporal.setCantidad(Integer.parseInt(info31[1]));
                temporal.setFecha_Cad(info31[2]);
                
                //añado el temporal en el ArrayList de productos actualizados 
                productosActualizados.add(temporal);
            }
        }  
        
        //actualiza bbdd con la informacion nueva 
        conexion.actualizarBBDD(productosActualizados);
    }//GEN-LAST:event_button_validar_pedidoActionPerformed

    //abre una ventana nueva para captar pedido nuevo 
    private void button_anyadir_pedidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_button_anyadir_pedidoActionPerformed
        
        label_stock_actual_actualizado.setVisible(false);
        //creo un vector con los productos existentes y relleno la tabla 
        String [] productos = {"TOMAHAWK", "FLAT IRON", "SUNDAY BEEF", "LAMB CHOPS", "SOLOMILLO TERNERA", 
                               "ENGLISH BFAST", "COSTILLAR BBQ", "CHURRASCO", "PS BURGER", "SALCHICHAS", 
                               "BEEF BURGER", "CHULETAS CORDERO"};
        info2 = new String[12][3];        
        
        for(int i = 0; i < info2.length; i++){
            info2[i][0] = productos[i];            
        }
        
        //actualizao la tabla con los nombres de los platos 
        tabla_ingredientes.setModel(new javax.swing.table.DefaultTableModel(
            info2,
            new String [] {
                "Nombre", "Cantidad", "Fecha_Cad"
            }
        ));
    }//GEN-LAST:event_button_anyadir_pedidoActionPerformed
    
    //capturación de stock e impresión 
    private void obtenerStock() {
        
        //devuelve las existencias de la bbdd con sus cantidades y respectivas fechas 
        ArrayList<productoStock> stock = new ArrayList<>();
        
        Conector_bbdd conexion = new Conector_bbdd();             
        
        //iguala el ArrayList al devuelto por la bbdd
        stock = conexion.obtenerStock();
                
        conexion.close();        
        
        //matriz donde guardo los datos de bbdd
        info = new String[stock.size()][3];
                
        for(int i = 0; i < stock.size(); i++){
            
            info[i][0] = stock.get(i).getNombre();
            info[i][1] = Integer.toString(stock.get(i).getCantidad());
            info[i][2] = stock.get(i).getFecha_Cad();          
        }
        
        //escribo en la tabla la información
        tabla_ingredientes.setModel(new javax.swing.table.DefaultTableModel(
            info,
            new String [] {
                "Nombre", "Cantidad", "Fecha_Cad"
            }
        ));
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton button_anyadir_pedido;
    private javax.swing.JButton button_back;
    private javax.swing.JButton button_validar_pedido;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel label_stock_actual_actualizado;
    private javax.swing.JTable tabla_ingredientes;
    // End of variables declaration//GEN-END:variables

}