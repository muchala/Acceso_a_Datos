
package proyectoescritorio;

public class Productos {
    
    private String nombre;
    private String salsa;
    private String punto;
    private String side;
    private int numero;
    public int numero_platos_repetidos = 1;

    public Productos(String nombre, String salsa, String punto, String side, int numero) {
        this.nombre = nombre;
        this.salsa = salsa;
        this.punto = punto;
        this.side = side;
        this.numero = numero;
        
    }
    public boolean equals (Object obj) {
     
        Productos temporal = (Productos) obj;

        return ((temporal.nombre.equals(this.nombre)) 
                && (temporal.salsa.equals(this.salsa)) 
                && (temporal.side.equals(this.side)) 
                && (temporal.punto.equals(this.punto)));        
    }
    
    public String uniqueString (){
        return this.nombre + " // " +
                this.salsa + " // " +
                this.side + " // " +
                this.punto; 
    }
    
    @Override
    public String toString() {
        return "Productos{" + "nombre=" + nombre + ", salsa=" + salsa + ", punto=" + punto 
                + ", side=" + side + ", numero=" + numero + ", numero_platos_repetidos=" + numero_platos_repetidos + "\n" +'}';
    }

    public int getNumero_platos_repetidos() {
        return numero_platos_repetidos;
    }

    public void setNumero_platos_repetidos(int numero) {
        this.numero_platos_repetidos +=  numero;
    }

    public Productos() {
    }

    public String getNombre() {
        return nombre;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSalsa() {
        return salsa;
    }

    public void setSalsa(String salsa) {
        this.salsa = salsa;
    }

    public String getPunto() {
        return punto;
    }

    public void setPunto(String punto) {
        this.punto = punto;
    }

    public String getSide() {
        return side;
    }

    public void setSide(String side) {
        this.side = side;
    }
    
}
