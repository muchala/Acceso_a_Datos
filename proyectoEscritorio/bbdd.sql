-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generaci�n: 11-05-2020 a las 11:52:38
-- Versi�n del servidor: 10.4.10-MariaDB
-- Versi�n de PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Base de datos: `ordenes`
--
CREATE DATABASE IF NOT EXISTS `ordenes` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ordenes`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleados`
--

CREATE TABLE `empleados` (
  `idEmpleado` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `codigo` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empleados`
--

INSERT INTO `empleados` (`idEmpleado`, `nombre`, `codigo`) VALUES
(1, 'admin', 'admin'),
(2, 'dam', '1234');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mesa`
--

CREATE TABLE `mesa` (
  `idMesa` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `salsa` varchar(10) NOT NULL,
  `punto` varchar(50) NOT NULL,
  `side` varchar(50) NOT NULL,
  `numero` varchar(2) NOT NULL,
  `atiende` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `idProducto` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `cantidad` int(4) NOT NULL,
  `fechaCaducidad` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`idProducto`, `nombre`, `cantidad`, `fechaCaducidad`) VALUES
(1, 'TOMAHAWK', 8, '45225'),
(2, 'FLAT IRON', 0, '/'),
(3, 'SUNDAY BEEF', 36, '21/12/0260'),
(4, 'LAMB CHOPS', 5, '20/205/502'),
(5, 'SOLOMILLO TERNERA', 0, '/'),
(6, 'ENGLISH BFAST', 0, '/'),
(7, 'COSTILLAR BBQ', 0, '/'),
(8, 'CHURRASCO', 56, 'muieee'),
(9, 'PS BURGER', 0, '/'),
(10, 'SALCHICHAS', 21, '21/112/121'),
(11, 'BEEF BURGER', 46, '55555'),
(12, 'CHULETAS CORDERO', 8, 'JOP');

--
-- �ndices para tablas volcadas
--

--
-- Indices de la tabla `empleados`
--
ALTER TABLE `empleados`
  ADD PRIMARY KEY (`idEmpleado`);

--
-- Indices de la tabla `mesa`
--
ALTER TABLE `mesa`
  ADD PRIMARY KEY (`idMesa`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`idProducto`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `empleados`
--
ALTER TABLE `empleados`
  MODIFY `idEmpleado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `mesa`
--
ALTER TABLE `mesa`
  MODIFY `idMesa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `idProducto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;
