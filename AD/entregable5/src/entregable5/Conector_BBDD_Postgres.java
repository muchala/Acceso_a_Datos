
package entregable5;

import java.math.BigInteger;
import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Conector_BBDD_Postgres {
    String url = "jdbc:postgresql://localhost:5432/postgres";
    String user = "postgres";
    String password = "1234";
    
    Connection conexion = null;
    
    public Conector_BBDD_Postgres(){
    
        try {

           conexion = DriverManager.getConnection(url, user, password);

            if(conexion != null){
                System.out.println("Conectado al servidor Postgres.");
            }
 
        }catch(SQLException ex){
            System.out.println("ERROR: No se a podido conectar con el servidor Postgres.");
            ex.printStackTrace();
        }
    }
    
    public ArrayList<Cliente> captar_tabla_clientes(){
       
        ArrayList<Cliente> clientes = new ArrayList();
        
        String sql_clientes ="SELECT * FROM clientes";      
        
        try {
            Class.forName("org.postgresql.Driver");
            java.sql.Statement st = conexion.createStatement();        
           
            ResultSet result_clientes = st.executeQuery(sql_clientes);                    
           
            while(result_clientes.next()){
                
                String nif = result_clientes.getString("nif");
                String nombre = result_clientes.getString("nombre");
                String apellidos = result_clientes.getString("apellidos");                
                BigInteger cc = new BigInteger(result_clientes.getString("cc"));                
                int cp = Integer.parseInt(result_clientes.getString("cp")); 
                
                clientes.add(new Cliente("", nif, nombre, apellidos, cc, cp));                
            }            
            st.close();
           
            } catch (SQLException ex) {
                Logger.getLogger(Conector_BBDD_Postgres.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
            Logger.getLogger(Conector_BBDD_Postgres.class.getName()).log(Level.SEVERE, null, ex);
        }
        return clientes;
    }
   
    public ArrayList<Poblaciones> captar_tabla_poblaciones(){
       
        ArrayList<Poblaciones> poblaciones = new ArrayList();
        
        String sql_poblaciones ="SELECT * FROM poblaciones";      
        
        try {
            Class.forName("org.postgresql.Driver");
            java.sql.Statement st = conexion.createStatement();        
           
            ResultSet result = st.executeQuery(sql_poblaciones);                    
           
            while(result.next()){
                
                int cp = Integer.parseInt(result.getString("codigo")); 
                String nombre = result.getString("nombre");                                                        
                
                poblaciones.add(new Poblaciones(nombre, cp));                
            }            
            st.close();
           
            } catch (SQLException ex) {
                Logger.getLogger(Conector_BBDD_Postgres.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
            Logger.getLogger(Conector_BBDD_Postgres.class.getName()).log(Level.SEVERE, null, ex);
        }
        return poblaciones;
    }
    
    public ArrayList<Cuenta_cliente> captar_tabla_cuentas() {
       
        ArrayList<Cuenta_cliente> cuentas = new ArrayList();
        
        String sql_cuentas ="SELECT * FROM cuentas";      
        
        try {
            Class.forName("org.postgresql.Driver");
            java.sql.Statement st = conexion.createStatement();        
           
            ResultSet result = st.executeQuery(sql_cuentas);                    
           
            while(result.next()){
                                
                BigInteger codigo = new BigInteger(result.getString("codigo"));                                                        
                String tipo = result.getString("tipo");                                                                                          
                double debe = Double.parseDouble(result.getString("debe"));
                double haber = Double.parseDouble(result.getString("haber"));
                String fecha = String.valueOf(result.getString("fecha"));                                           
                
                Cuenta_cliente cuentaTemporal = new Cuenta_cliente(tipo);
                                            
                cuentas.add(cuentaTemporal);                                
               
                Anotaciones anotacionTemporal = new Anotaciones(codigo ,fecha, debe, haber);
                cuentaTemporal.aniadirAnotacion(anotacionTemporal);                 
            }                    
          
            st.close();
           
            } catch (SQLException ex) {
                Logger.getLogger(Conector_BBDD_Postgres.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
            Logger.getLogger(Conector_BBDD_Postgres.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cuentas;
    }
    
    public void close(){
        try {
            conexion.close();
        } catch (SQLException ex) {
            Logger.getLogger(Conector_BBDD_Postgres.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}

