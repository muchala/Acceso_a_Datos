
package entregable5;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Conector_BBDD_Mysql {
    
    Connection conexion = null;
    
    public Conector_BBDD_Mysql(){

        try{

            String url = "jdbc:mysql://localhost:3306/";
            String user = "root";
            String password = "";

            conexion = DriverManager.getConnection(url, user, password);

            if(conexion != null){
                System.out.println("Conectado al servidor PHP.");
            }

        }catch(SQLException ex){
            System.out.println("ERROR: No se a podido conectar con el servidor.");
            ex.printStackTrace();
        }
    }
    
    
    public void crear_BBDD_PHP(String archivo){

        String consulta = "", linea;

        try{

            FileReader f = new FileReader(archivo);
            BufferedReader br = new BufferedReader(f);
            Statement stmt = conexion.createStatement();

            do{

                linea = br.readLine();

                if(linea != null){
                    if(!linea.equals("") && !linea.substring(0,1).equals("-")){

                        if(!linea.substring((linea.length()-1), linea.length()).equals(";")){
                            consulta += linea;
                        }else{
                            consulta += linea;
                            stmt.executeUpdate(consulta);                            
                            consulta = "";
                        }
                    }
                }
                
            }while(linea != null);
            br.close();
            
        }catch (SQLException ex) {
            System.out.println("ERROR: Al hacer un Insert");
            ex.printStackTrace();
        } catch (IOException ex) {
            Logger.getLogger(Conector_BBDD_Mysql.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void insertar_poblaciones_PHP(ArrayList<Poblaciones>poblaciones){
         
        String query = "INSERT INTO poblaciones(`codigo`, `nombre`) VALUES(?,?)";
               
        try{
                        
            PreparedStatement ps = conexion.prepareStatement(query);
            
            for(int i = 0; i < poblaciones.size(); i++){             
                
               ps.setString(1, Integer.toString(poblaciones.get(i).getId()));
               ps.setString(2, poblaciones.get(i).getNombre());
               
               ps.executeUpdate();              
            }
            
            System.out.println("Poblaciones insertadas correctamente");
            
            ps.close();
            
        }catch(SQLException ex) {
            System.out.println("ERROR: Al hacer un insert en poblaciones" + ex.toString());
        }
    
    
    }
     
    public void insertar_datos_fiscales(ArrayList<Cliente>clientes){       
         
        String query = "INSERT INTO datos_fiscales(`nif`, `nombre`, `apellidos`, `cp`, `cc`) VALUES(?,?,?,?,?)";
               
        try{
            
            
            PreparedStatement ps = conexion.prepareStatement(query);
            
            for(int i = 0; i < clientes.size(); i++){             
                
               ps.setString(1, clientes.get(i).getNif());
               ps.setString(2, clientes.get(i).getNombre());
               ps.setString(3, clientes.get(i).getApellidos());
               ps.setString(4, Integer.toString(clientes.get(i).getCp()));
               ps.setString(5, String.valueOf(clientes.get(i).getCc()));
               
               ps.executeUpdate();              
            }
            
            System.out.println("Datos Fiscales insertados correctamente");
            
            ps.close();
            
        }catch(SQLException ex) {
            System.out.println("ERROR: Al hacer un insert en poblaciones" + ex.toString());
        }
    }
    
    public void insertar_clientes(ArrayList<Cliente>clientes){
       
        String query = "INSERT INTO clientes(`dato_fiscal`, `telf_contacto`) VALUES(?,?)";
           
        try{
            
            //String query2 = "";   
            PreparedStatement ps = conexion.prepareStatement(query);
            long clave_ajena = 0;
                    
            for(int i = 0; i < clientes.size(); i++){  
                
                //intento guardar el codigo de datos_fiscales en una variable nueva con una consulta , pero me da errores de conversión 
                  try{    
                    String query2 = "SELECT codigo FROM datos_fiscales WHERE  nif = \'"+clientes.get(i).getNif()+"\'";
                   
                    Statement stmt = conexion.createStatement();           
                    
                    ResultSet result = stmt.executeQuery(query2);
                    
                    while(result.next()){
                        clave_ajena = result.getLong("codigo");
                    }                
                    
                    stmt.close();
                    result.close();

                }catch (SQLException ex) {
                    System.out.println("ERROR: al extraer codigo de datos fiscales");
                    ex.printStackTrace();
                }         
               
                              
               ps.setLong(1, clave_ajena);
               ps.setString(2, clientes.get(i).getTelefono());              
               
               ps.executeUpdate();              
            }
            
            System.out.println("Clientes insertados correctamente");
            
            ps.close();
            
        }catch(SQLException ex) {
            System.out.println("ERROR: Al hacer un insert en poblaciones" + ex.toString());
        }
    }
      
    public void insertar_cuentas_PHP(ArrayList<Cuenta_cliente>cuentas){
        
        String query = "INSERT INTO cuenta_cliente(`tipo`, `saldo`) VALUES(?,?)";
        
        try{
                        
            PreparedStatement ps = conexion.prepareStatement(query);
            
            for(int i = 0; i < cuentas.size(); i++){  
                          
               ps.setString(1, cuentas.get(i).getTipo());
               ps.setDouble(2, cuentas.get(i).getSaldo());
               
               ps.executeUpdate();              
            }
            
            System.out.println("Cuentas cliente insertadas correctamente");
            
            ps.close();
            
        }catch(SQLException ex) {
            System.out.println("ERROR: Al hacer un insert en poblaciones" + ex.toString());
        }        
    }    
    
    public void insertar_anotaciones(ArrayList<Cuenta_cliente>cuentas){
         
        String query = "INSERT INTO anotaciones(`cc`, `fecha`, `debe`, `haber`) VALUES(?,?,?,?)";
               
        ArrayList<Anotaciones> temporal = new ArrayList<>();       
       
        try{
                        
            PreparedStatement ps = conexion.prepareStatement(query);
            
            for(int j = 0; j < cuentas.size(); j++){  
                temporal = cuentas.get(j).getAnotaciones();                          

                for(int i = 0; i < temporal.size(); i++){  

                    ps.setString(1, String.valueOf(temporal.get(i).getCc()));
                    ps.setString(2,  temporal.get(i).getFecha());
                    ps.setDouble(3, temporal.get(i).getDebe());
                    ps.setDouble(4, temporal.get(i).getHaber());

                    ps.executeUpdate();              
                }
            }
            System.out.println("Anotaciones insertadas correctamente");
            
            ps.close();
            
        }catch(SQLException ex) {
            System.out.println("ERROR: Al hacer un insert en poblaciones" + ex.toString());
        }                
    }
    
    public void close(){

       try{

           conexion.close();
           System.out.println("Conexion cerrada.");

       }catch(SQLException ex){
           System.out.print("ERROR: Al cerrar la conexión.");
           ex.printStackTrace();
       }

    }

}

