
package entregable5;

import java.math.BigInteger;

public class Cliente extends Datos_Fiscales{
    String telefono;

    public Cliente(String telefono, String nif, String nombre, String apellidos, BigInteger cc, int cp) {
        super(nif, nombre, apellidos, cc, cp);
        this.telefono = telefono;
    }

    @Override
    public String toString() {
        return "Cliente{\n\t" + super.toString()+ " " +  "telefono=" + telefono + '}';
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }    
}
