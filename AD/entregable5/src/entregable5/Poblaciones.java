
package entregable5;

public class Poblaciones {
    private String nombre;
    private int id;

    public Poblaciones(String nombre, int id) {
        this.nombre = nombre;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Poblaciones{" + "nombre=" + nombre + ", id=" + id + '}';
    }
    
    
}
