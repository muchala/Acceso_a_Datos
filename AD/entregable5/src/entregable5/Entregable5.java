
package entregable5;

import java.util.ArrayList;


public class Entregable5 {
    
    public static void main(String[] args) {
        
        Conector_BBDD_Postgres conexion_Postgres = new Conector_BBDD_Postgres();
        Conector_BBDD_Mysql conexion_PHP = new Conector_BBDD_Mysql();
        
        ArrayList<Cliente>clientes = new ArrayList<>();
        ArrayList<Poblaciones>poblaciones = new ArrayList<>();
        ArrayList<Cuenta_cliente>cuentas = new ArrayList<>();
                       
        //metodos acceso a bbdd Postgres y  descargado de información
        clientes = conexion_Postgres.captar_tabla_clientes();
        cuentas = conexion_Postgres.captar_tabla_cuentas();
        poblaciones = conexion_Postgres.captar_tabla_poblaciones();
        
        //lectura archivo txt y creación de bbdd en PHP
        conexion_PHP.crear_BBDD_PHP("audiogil.sql");
                
        //insert en las tablas de la bbdd en PHP        
        conexion_PHP.insertar_poblaciones_PHP(poblaciones);            
        conexion_PHP.insertar_datos_fiscales(clientes);
        conexion_PHP.insertar_clientes(clientes);
        conexion_PHP.insertar_cuentas_PHP(cuentas);              
        conexion_PHP.insertar_anotaciones(cuentas);
                
        //cerrar conexiónes
        conexion_PHP.close();
        conexion_Postgres.close();
        
    }    
}
