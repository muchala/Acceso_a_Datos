
package entregable5;

import java.math.BigInteger;

public class Anotaciones {
 
  private BigInteger cc;
  private String fecha;
  private double debe;
  private double haber;

    public Anotaciones(BigInteger cc, String fecha, double debe, double haber) {
        this.cc = cc;
        this.fecha = fecha;
        this.debe = debe;
        this.haber = haber;
    }

    public Anotaciones() {
    }

    public BigInteger getCc() {
        return cc;
    }

    public void setCc(BigInteger cc) {
        this.cc = cc;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public double getDebe() {
        return debe;
    }

    public void setDebe(double debe) {
        this.debe = debe;
    }

    public double getHaber() {
        return haber;
    }

    public void setHaber(double haber) {
        this.haber = haber;
    }

    @Override
    public String toString() {
        return "Anotaciones{" + "cc=" + cc + ", fecha=" + fecha + ", debe=" + debe + ", haber=" + haber + '}';
    }
  
}
