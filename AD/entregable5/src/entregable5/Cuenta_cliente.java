
package entregable5;

import java.util.ArrayList;

public class Cuenta_cliente {
    
    private String tipo;    
    private double saldo = 0;
    private ArrayList<Anotaciones> anotaciones;

    public Cuenta_cliente(String tipo) {
        this.tipo = tipo;          
        this.anotaciones = new ArrayList();
    }

    public ArrayList<Anotaciones>getAnotaciones() {
        return anotaciones;
    }

    public void aniadirAnotacion(Anotaciones anotacion) {
        this.anotaciones.add(anotacion);
        saldo += anotacion.getHaber() - anotacion.getDebe();
    }
   
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    @Override
    public String toString() {     
        return "Cuenta_cliente{" + ", \n\ttipo=" + tipo + ", \n\tsaldo=" + saldo +"\n\t" +anotaciones.toString()+ '}';
    }
    
}
