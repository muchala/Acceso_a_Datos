
package entregable5;

import java.math.BigInteger;

public class Datos_Fiscales {
    private String nif;
    private String nombre;
    private String apellidos;
    private BigInteger cc;
    private int cp;

    public Datos_Fiscales(String nif, String nombre, String apellidos, BigInteger cc, int cp) {
        this.nif = nif;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.cc = cc;
        this.cp = cp;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public BigInteger getCc() {
        return cc;
    }

    public void setCc(BigInteger cc) {
        this.cc = cc;
    }

    @Override
    public String toString() {
        return "nif=" + nif + ", nombre=" + nombre + ", apellidos=" + apellidos + ", cc=" + cc + ", cp=" + cp + '}';
    }

    public int getCp() {
        return cp;
    }

    public void setCp(int cp) {
        this.cp = cp;
    }
    
}
