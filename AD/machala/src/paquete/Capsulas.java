
package paquete;


public class Capsulas extends javax.swing.JFrame {

   
    
    private Menu enlaceMenu;
    
    public Capsulas(Menu menu) {
        initComponents();
        this.setLocationRelativeTo(null);
        GroupCapsulas.add(RadioButtonNr2);
        GroupCapsulas.add(RadioButtonNr4);
        enlaceMenu = menu;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        GroupCapsulas = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        RadioButtonNr2 = new javax.swing.JRadioButton();
        RadioButtonNr4 = new javax.swing.JRadioButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        ButtonAñadir = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        NombreIngrediente = new javax.swing.JTextField();
        CantidadMG = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        TablaIngredientes = new javax.swing.JTable();
        ComboBoxRelleno = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        NumeroCapsula = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        CapsulaCalcular = new javax.swing.JButton();
        CapsulaCancelar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("CAPSULAS");

        RadioButtonNr2.setText("Nr2");
        RadioButtonNr2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RadioButtonNr2ActionPerformed(evt);
            }
        });

        RadioButtonNr4.setText("Nr4");

        jLabel2.setText("TAMAÑO:");

        jLabel6.setText("mg");

        ButtonAñadir.setText("Añadir");
        ButtonAñadir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonAñadirActionPerformed(evt);
            }
        });

        jLabel4.setText("Ingrediente");

        NombreIngrediente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NombreIngredienteActionPerformed(evt);
            }
        });

        CantidadMG.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CantidadMGActionPerformed(evt);
            }
        });

        TablaIngredientes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Nombre", "Cantidad Gramos", "Precio"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Float.class, java.lang.Float.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane1.setViewportView(TablaIngredientes);
        if (TablaIngredientes.getColumnModel().getColumnCount() > 0) {
            TablaIngredientes.getColumnModel().getColumn(1).setMinWidth(100);
            TablaIngredientes.getColumnModel().getColumn(1).setMaxWidth(110);
            TablaIngredientes.getColumnModel().getColumn(2).setMinWidth(100);
            TablaIngredientes.getColumnModel().getColumn(2).setMaxWidth(110);
        }

        ComboBoxRelleno.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Elige opcion", "Excipiente nr1", "Excipiente nr2", "Lactosa" }));
        ComboBoxRelleno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ComboBoxRellenoActionPerformed(evt);
            }
        });

        jLabel7.setText("Relleno:");

        NumeroCapsula.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NumeroCapsulaActionPerformed(evt);
            }
        });

        jLabel3.setText("Numero capsulas:");

        CapsulaCalcular.setText("Calcular");
        CapsulaCalcular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CapsulaCalcularActionPerformed(evt);
            }
        });

        CapsulaCancelar.setText("Cancelar");
        CapsulaCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CapsulaCancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(NombreIngrediente, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(38, 38, 38)
                                .addComponent(CantidadMG, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel6))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(61, 61, 61)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(ButtonAñadir)
                        .addGap(28, 28, 28))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(RadioButtonNr2)
                        .addGap(18, 18, 18)
                        .addComponent(RadioButtonNr4)
                        .addGap(50, 50, 50)
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ComboBoxRelleno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(NumeroCapsula, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(123, 123, 123)
                                .addComponent(CapsulaCalcular)
                                .addGap(81, 81, 81)
                                .addComponent(CapsulaCancelar)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(jLabel1)
                .addGap(36, 36, 36)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(RadioButtonNr2)
                    .addComponent(RadioButtonNr4)
                    .addComponent(ComboBoxRelleno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ButtonAñadir)
                    .addComponent(jLabel4)
                    .addComponent(NombreIngrediente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CantidadMG, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(NumeroCapsula, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CapsulaCalcular)
                    .addComponent(CapsulaCancelar))
                .addContainerGap(15, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ButtonAñadirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonAñadirActionPerformed

        boolean encontrado = false;
        String[][] info;
        
        
        for(int i = 0; i < enlaceMenu.ingredientes.size() && !encontrado; i++){
            if(NombreIngrediente.getText().equals(enlaceMenu.ingredientes.get(i).getNombre())){
                encontrado = true;
            }
            
            if(encontrado){
                enlaceMenu.resumen.añadirIngredientes(enlaceMenu.ingredientes.get(i), (Double.parseDouble(CantidadMG.getText())/1000));
            }
        }
        
        info = new String[enlaceMenu.resumen.getIngredeFinales().size()][3];

        for(int i = 0; i < enlaceMenu.resumen.getIngredeFinales().size(); i++){
            
            info[i][0] = enlaceMenu.resumen.getIngredeFinales().get(i).getNombre();
            info[i][1] = Double.toString(enlaceMenu.resumen.getPesoIngredientes().get(i));
            info[i][2] = Double.toString(enlaceMenu.resumen.getPesoIngredientes().get(i) * enlaceMenu.resumen.getIngredeFinales().get(i).getPrecio());
            System.out.println("Precio total de " + enlaceMenu.resumen.getIngredeFinales().get(i) + ": " + info[i][2]);
        }
        
        TablaIngredientes.setModel(new javax.swing.table.DefaultTableModel(
            info,
            new String [] {
                "Nombre", "Cantidad", "Precio"
            }
        ));
        
        NombreIngrediente.setText("");
        CantidadMG.setText("");
    }//GEN-LAST:event_ButtonAñadirActionPerformed

    private void NombreIngredienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NombreIngredienteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_NombreIngredienteActionPerformed

    private void CantidadMGActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CantidadMGActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CantidadMGActionPerformed

    private void NumeroCapsulaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NumeroCapsulaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_NumeroCapsulaActionPerformed

    private void CapsulaCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CapsulaCancelarActionPerformed
        enlaceMenu.setVisible(true);
        dispose();
    }//GEN-LAST:event_CapsulaCancelarActionPerformed

    private void ComboBoxRellenoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ComboBoxRellenoActionPerformed
    }//GEN-LAST:event_ComboBoxRellenoActionPerformed

    private void CapsulaCalcularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CapsulaCalcularActionPerformed

        int indexCombo = 0;
        double capacidadTotal = 0, rellenoPrecio = 0, pesoIngredientes = 0, 
                pesoRelleno = 0, precioCapsula = 0, precioIngredientes = 0;

        if(RadioButtonNr2.isSelected() == true){
            capacidadTotal = 0.00037;
        }else if(RadioButtonNr4.isSelected() == true){
            capacidadTotal = 0.0002;
        }

        indexCombo = ComboBoxRelleno.getSelectedIndex();

        switch(indexCombo){
            case 1:
                rellenoPrecio = 0.05;
                break;
            case 2:
                rellenoPrecio = 0.04;
                break;
            case 3:
                rellenoPrecio = 0.021937;
                break;
        }

        for(int i =0; i < enlaceMenu.resumen.getPesoIngredientes().size(); i++){
            pesoIngredientes += enlaceMenu.resumen.getPesoIngredientes().get(i);
            precioIngredientes += enlaceMenu.resumen.getIngredeFinales().get(i).getPrecio();
        }
        System.out.println("Precio de relleno unitario: " + rellenoPrecio);
        pesoRelleno = capacidadTotal - pesoIngredientes;
        rellenoPrecio = pesoRelleno * rellenoPrecio;

        precioCapsula = rellenoPrecio + precioIngredientes;
        enlaceMenu.resumen.setPrecioTotalCapsulas(precioCapsula * Integer.parseInt(NumeroCapsula.getText()));
        
        System.out.println("Capacidad total: " + capacidadTotal);
        System.out.println("Peso total de ingredientes: " + pesoIngredientes);
        System.out.println("Precio total de ingredientes: " + precioIngredientes);
        System.out.println("Precio total de relleno: " + rellenoPrecio);
        System.out.println("Peso total de relleno: " + pesoRelleno);
        System.out.println("Precio de capsula unitaria: " + precioCapsula);
        System.out.println("Precio total pastillas: " + enlaceMenu.resumen.getPrecioTotalCapsulas()/1000);

    }//GEN-LAST:event_CapsulaCalcularActionPerformed

    private void RadioButtonNr2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RadioButtonNr2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_RadioButtonNr2ActionPerformed

    /**
     * @param args the command line arguments
     */
    /*public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        /*try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Capsulas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Capsulas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Capsulas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Capsulas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        /*java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Capsulas().setVisible(true);
            }
        });
    }*/

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ButtonAñadir;
    private javax.swing.JTextField CantidadMG;
    private javax.swing.JButton CapsulaCalcular;
    private javax.swing.JButton CapsulaCancelar;
    private javax.swing.JComboBox ComboBoxRelleno;
    private javax.swing.ButtonGroup GroupCapsulas;
    private javax.swing.JTextField NombreIngrediente;
    private javax.swing.JTextField NumeroCapsula;
    private javax.swing.JRadioButton RadioButtonNr2;
    private javax.swing.JRadioButton RadioButtonNr4;
    private javax.swing.JTable TablaIngredientes;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
