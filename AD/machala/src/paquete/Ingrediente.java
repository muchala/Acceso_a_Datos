package paquete;

public class Ingrediente {

    private String nombre;
    private double precio;
    private String financiacion="";
    private String aportacion="";
    private String comentario="";
    private String via="";
    private String envase="";
    
    public Ingrediente(String nombre, double precio) {
        this.nombre = nombre;
        this.precio = precio;
    }

    public String getNombre() {
        return nombre;
    }

    public String getFinanciacion() {
        return financiacion;
    }

    public String getAportacion() {
        return aportacion;
    }

    public String getComentario() {
        return comentario;
    }

    public String getVia() {
        return via;
    }

    public String getEnvase() {
        return envase;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        return "Ingrediente{" + "nombre=" + nombre + ", precio=" + precio + '}';
    }

}
