package paquete;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class ResumenTotal {

    private ArrayList<Ingrediente> ingredeFinales;
    private List<Double> pesoIngredientes;
    private double precioTotalCapsulas;
    private double precioEnvase;
    private double precioMermas;
    private double precioHonorarios;

    public ResumenTotal() {
        ingredeFinales = new ArrayList();
        pesoIngredientes = new ArrayList<>(Arrays.asList());
    }

    public void añadirIngredientes(Ingrediente ingrediente, double peso){
        ingredeFinales.add(ingrediente);
        añadirPeso(peso);
    }

    private void añadirPeso(double peso){
        pesoIngredientes.add(peso);
    }

    public void calcularMermas(){

        double precio = 0;
        
        for(int i = 0; i < ingredeFinales.size(); i++){

            precio += ingredeFinales.get(i).getPrecio() * pesoIngredientes.get(i);
        
        }
        precio = precio * 0.02;
        precioMermas = precio;

    }
    public ArrayList<Ingrediente> getIngredeFinales() {
        return ingredeFinales;
    }

    public void setIngredeFinales(ArrayList<Ingrediente> ingredeFinales) {
        this.ingredeFinales = ingredeFinales;
    }

    public List<Double> getPesoIngredientes() {
        return pesoIngredientes;
    }

    public void setPesoIngredientes(List<Double> pesoIngredientes) {
        this.pesoIngredientes = pesoIngredientes;
    }

    public double getPrecioEnvase() {
        return precioEnvase;
    }

    public void setPrecioEnvase(double precioEnvase) {
        this.precioEnvase = precioEnvase;
    }

    public double getPrecioMermas() {
        return precioMermas;
    }

    public void setPrecioMermas(double precioMermas) {
        this.precioMermas = precioMermas;
    }

    public double getPrecioHonorarios() {
        return precioHonorarios;
    }

    public void setPrecioHonorarios(double precioHonorarios) {
        this.precioHonorarios = precioHonorarios;
    }

    public double getPrecioTotalCapsulas() {
        return precioTotalCapsulas;
    }

    public void setPrecioTotalCapsulas(double precioTotalCapsulas) {
        this.precioTotalCapsulas = precioTotalCapsulas;
    }
}
