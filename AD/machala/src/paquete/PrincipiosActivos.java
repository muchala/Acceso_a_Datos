package paquete;

public class PrincipiosActivos extends Ingrediente{

    private String financiacion;
    private String aportacion;
    private String comentario;
    private String via;
    private String envase;
    
    public PrincipiosActivos(String nombre, String financiacion, String aportacion, String comentario, String via, String envase,  double precio) {
        super(nombre, precio);
        this.financiacion = financiacion;
        this.aportacion = aportacion;
        this.comentario = comentario;
        this.via = via;
        this.envase = envase;
    }
    
    public String getFinanciacion() {
        return financiacion;
    }

    public void setFinanciacion(String financiacion) {
        this.financiacion = financiacion;
    }

    public String getAportacion() {
        return aportacion;
    }

    public void setAportacion(String aportacion) {
        this.aportacion = aportacion;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getVia() {
        return via;
    }

    public void setVia(String via) {
        this.via = via;
    }

    public String getEnvase() {
        return envase;
    }

    public void setEnvase(String envase) {
        this.envase = envase;
    }

    @Override
    public String toString() {
        return "PrincipiosActivos{"+"Nombre "+super.getNombre() + "financiacion=" + financiacion + ", aportacion=" + aportacion + ", comentario=" + comentario + ", via=" + via + ", envase=" + envase + "precio "+super.getPrecio()+'}';
    }

}