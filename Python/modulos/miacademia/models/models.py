# -*- coding: utf-8 -*-

from odoo import models, fields, api

class Curso(models.Model):
     _name = 'miacademia.curso'
     _rec_name = 'nombre'

     nombre = fields.Char(string='Nombre', required=True)
     description = fields.Text(string='Descripcion del curso')


class Edicion(models.Model):
     _name = 'miacademia.edicion'