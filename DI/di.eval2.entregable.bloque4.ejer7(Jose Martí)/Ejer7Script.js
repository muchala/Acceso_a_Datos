$(function(){
    var tablaJson;

    $.getJSON('Libros.json', function(data){

        tablaJson = data.libros;

        mostrarTabla();

    })

    function mostrarTabla(){
        
        var mostrar = "<tr><th>Título</th><th>Autor</th><th>Nº Paginas</th><th>Editorial</th></tr>";

        jQuery.each(tablaJson, function(i, libro){

            mostrar += "<tr>";

            $.each(libro, function(index, valor){

                if(index == "númeroPáginas"){
                    mostrar = mostrar + "<td class=\'centrar\'>" + valor + "</td>";
                }else{
                    mostrar = mostrar + "<td class=\'uncenter\'>" + valor + "</td>";
                }
            });
            
            mostrar += "</tr>";
            console.log(mostrar);

        });

        $("#tabla").append(mostrar);

    }

});